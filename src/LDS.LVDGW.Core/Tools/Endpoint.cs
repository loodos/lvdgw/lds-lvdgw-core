﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Tools
{
    public class Endpoint
    {
        private string _verb;
        private string _pathRegexPattern;

        public Endpoint(string pathRegexPattern, string verb = "*")
        {
            _verb = verb;
            _pathRegexPattern = pathRegexPattern;
        }

        public bool IsMatch(string path, string verb)
        {
            if (_verb == "*" || _verb.Equals(verb, StringComparison.InvariantCultureIgnoreCase))
            {
                if (Regex.IsMatch(path, _pathRegexPattern))
                {
                    return true;
                }
            }

            return false;
        }

        public override string ToString()
        {
            return $"{_verb}:{_pathRegexPattern}";
        }

        public static Endpoint Parse(string endpointString)
        {
            if (string.IsNullOrEmpty(endpointString))
                throw new ArgumentNullException(nameof(endpointString));

            var values = endpointString.Split(':', 2, StringSplitOptions.RemoveEmptyEntries);
            var verb = "*";
            var pathRegexPattern = "";

            if (values.Length == 2)
            {
                verb = values[0]?.Trim();
                pathRegexPattern = values[1]?.Trim();
            }
            else
            {
                pathRegexPattern = values[0]?.Trim();
            }

            return new Endpoint(pathRegexPattern, verb);
        }
    }
}
