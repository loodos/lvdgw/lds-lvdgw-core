﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Tools
{
    /// https://github.com/jsakamoto/ipaddressrange

    public static class IPUtil
    {
        public static IPAddress GetClientIp(HttpContext httpContext, string _realIpHeader = null)
        {
            if (!string.IsNullOrEmpty(_realIpHeader) && httpContext.Request.Headers.Keys.Contains(_realIpHeader, StringComparer.CurrentCultureIgnoreCase))
            {
                return ParseIp(httpContext.Request.Headers[_realIpHeader].Last());
            }

            return httpContext.Connection.RemoteIpAddress;
        }

        public static IPAddress ParseIp(string ipAddress)
        {
            //remove port number from ip address if any
            ipAddress = ipAddress.Split(',').First().Trim();
            int portDelimiterPos = ipAddress.LastIndexOf(":", StringComparison.CurrentCultureIgnoreCase);
            bool ipv6WithPortStart = ipAddress.StartsWith("[");
            int ipv6End = ipAddress.IndexOf("]");
            if (portDelimiterPos != -1
                && portDelimiterPos == ipAddress.IndexOf(":", StringComparison.CurrentCultureIgnoreCase)
                || ipv6WithPortStart && ipv6End != -1 && ipv6End < portDelimiterPos)
            {
                ipAddress = ipAddress.Substring(0, portDelimiterPos);
            }

            return IPAddress.Parse(ipAddress);
        }

        public static bool ContainsIp(string[] IPRangeList, IPAddress IP)
        {
            if (IPRangeList != null && IPRangeList.Any())
            {
                foreach (var IPRange in IPRangeList)
                {
                    var range = NetTools.IPAddressRange.Parse(IPRange);
                    if (range.Contains(IP))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        public static bool ContainsIp(string IPRange, IPAddress IP)
        {
            return ContainsIp(new string[] { IPRange }, IP);
        }

    }
}
