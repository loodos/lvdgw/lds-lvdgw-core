﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using AspNetCoreRateLimit;
using LDS.JsonLocalizer;
using LDS.LVDGW.Core.Extensions;
using LDS.LVDGW.Core.Middlewares;
using LDS.LVDGW.Core.Services;
using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Plugin.Enrichment;
using LDS.LVDGW.Plugin.Microservice;
using LDS.LVDGW.Plugin.ResponseWrapper;
using LDS.LVDGW.Plugin.UserAuthentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LDS.LVDGW.Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Json Global Configuration
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                //,NullValueHandling = NullValueHandling.Ignore
            };

            services.AddOptions();
            services.Configure<AppSettings>(Configuration);

            services.AddCors(options =>
            {
                options.AddPolicy("EnableCORS", builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials().Build();
                });
            });

            var serviceProvider = services.BuildServiceProvider();
            var appSettings = serviceProvider.GetService<IOptions<AppSettings>>()?.Value ?? throw new ArgumentNullException(nameof(AppSettings));

            services.AddJsonLocalization(options => options.JsonResourcesPath = appSettings.Localization.JsonResourcesPath);

            #region Cache
            bool? isCacheServiceDistributed = null;
            if (appSettings.Cache.MemoryCache.Enable)
            {
                services.AddMemoryCache();

                isCacheServiceDistributed = false;
                services.AddSingleton<ICacheService, MemoryCacheService>();
            }
            else if (appSettings.Cache.DistributedRedisCache.Enable)
            {
                services.AddDistributedRedisCache(options => {
                    options.Configuration = appSettings.Cache.DistributedRedisCache.Options["Configuration"]?.ToString();
                    options.InstanceName = appSettings.Cache.DistributedRedisCache.Options["InstanceName"]?.ToString();
                });

                isCacheServiceDistributed = true;
                services.AddSingleton<ICacheService, DistributedCacheService>();
            }
            else if (appSettings.Cache.DistributedSqlServerCache.Enable)
            {
                services.AddDistributedSqlServerCache(options => {
                    options.ConnectionString = appSettings.Cache.DistributedSqlServerCache.Options["ConnectionString"]?.ToString();
                    options.SchemaName = appSettings.Cache.DistributedSqlServerCache.Options["SchemaName"]?.ToString();
                    options.TableName = appSettings.Cache.DistributedSqlServerCache.Options["TableName"]?.ToString();
                });

                isCacheServiceDistributed = true;
                services.AddSingleton<ICacheService, DistributedCacheService>();
            }

            if (appSettings.ResponseCache.Enable)
            {
                if (appSettings.ResponseCache.CacheStore == CacheSettings.CacheStores.MemoryCache)
                {
                    if (!isCacheServiceDistributed.HasValue || isCacheServiceDistributed.Value)
                        throw new NotSupportedException("ResponseCache.CacheStore=MemoryCache not supported. Invalid configuration!");
                }
                else
                {
                    if (!isCacheServiceDistributed.HasValue || !isCacheServiceDistributed.Value)
                        throw new NotSupportedException("ResponseCache.CacheStore=DistributedCache not supported. Invalid configuration!");
                }
            }
            #endregion

            #region Plugins
            var pluginService = new PluginService(services, appSettings.Plugins);

            if (appSettings.UserAuthentication.Enable)
            {
                pluginService.AddPluginService<IUserAuthenticationPlugin>(appSettings.UserAuthentication.Plugin);
            }

            if (appSettings.ResponseWrapper.Enable)
            {
                pluginService.AddPluginService<IResponseWrapperPlugin>(appSettings.ResponseWrapper.Plugin);
            }

            if (appSettings.Enrichment.Plugins != null && appSettings.Enrichment.Plugins.Any())
            {
                foreach (var plugin in appSettings.Enrichment.Plugins)
                {
                    pluginService.AddPluginService<IEnrichmentPlugin>(plugin);
                }
            }

            appSettings.Microservices.Where(x => x.Enable).Select(x => x.Plugin).ToList().ForEach(plugin => {
                pluginService.AddPluginService<IMicroservicePlugin>(plugin);
            });
            #endregion

            #region RateLimit with [AspNetCoreRateLimit]
            if (appSettings.RateLimit.Enable)
            {
                if (appSettings.RateLimit.IpRateLimiting.Enable)
                {
                    services.Configure<IpRateLimitOptions>(Configuration.GetSection("RateLimit:IpRateLimiting:Options"));
                    services.Configure<IpRateLimitPolicies>(Configuration.GetSection("RateLimit:IpRateLimiting:Policies"));
                }
                if (appSettings.RateLimit.ClientRateLimiting.Enable)
                {
                    services.Configure<ClientRateLimitOptions>(Configuration.GetSection("RateLimit:ClientRateLimiting:Options"));
                    services.Configure<ClientRateLimitPolicies>(Configuration.GetSection("RateLimit:ClientRateLimiting:Policies"));
                }

                if (appSettings.RateLimit.CacheStore == CacheSettings.CacheStores.MemoryCache)
                {
                    if (!isCacheServiceDistributed.HasValue || isCacheServiceDistributed.Value)
                        throw new NotSupportedException("RateLimit.CacheStore=MemoryCache not supported. Invalid configuration!");
                    
                    if (appSettings.RateLimit.IpRateLimiting.Enable)
                        services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
                    if (appSettings.RateLimit.ClientRateLimiting.Enable)
                        services.AddSingleton<IClientPolicyStore, MemoryCacheClientPolicyStore>();

                    services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
                }
                else // use DistributedCache
                {
                    if (!isCacheServiceDistributed.HasValue || !isCacheServiceDistributed.Value)
                        throw new NotSupportedException("RateLimit.CacheStore=DistributedCache not supported. Invalid configuration!");

                    if (appSettings.RateLimit.IpRateLimiting.Enable)
                        services.AddSingleton<IIpPolicyStore, DistributedCacheIpPolicyStore>();
                    if (appSettings.RateLimit.ClientRateLimiting.Enable)
                        services.AddSingleton<IClientPolicyStore, DistributedCacheClientPolicyStore>();

                    services.AddSingleton<IRateLimitCounterStore, DistributedCacheRateLimitCounterStore>();
                }
            }
            #endregion

            if (appSettings.PublicFileServer.Enable && appSettings.PublicFileServer.EnableDirectoryBrowsing)
            {
                services.AddDirectoryBrowser();
            }

            // If MetricsMonitoring and ReportTo enabled, run background metrics service
            if (appSettings.MetricsMonitoring.Enable && appSettings.MetricsMonitoring.ReportTo.Enable)
            {
                services.AddMetricsReportingHostedService();
            }

            services.AddRouting();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IOptions<AppSettings> appSettings)
        {
            app.UseCors("EnableCORS");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var settings = appSettings?.Value ?? throw new ArgumentNullException(nameof(AppSettings));

            /// TODO list
            /// BUG: If service response ContentType is text/plain, on responsecache middleware can't convert to application/json
            /// Documantation: Swagger integration is possible?
            /// Safe storage app secrets - secret manager (https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-2.1&tabs=linux)

            if (settings.Localization.Enable)
            {
                var supportedCultures = settings.Localization.SupportedCultures.Select(culture => new CultureInfo(culture)).ToList();

                //app.UseRequestLocalization(new RequestLocalizationOptions
                //{
                //    DefaultRequestCulture = new RequestCulture(settings.Localization.DefaultCulture),
                //    SupportedCultures = supportedCultures,
                //    SupportedUICultures = supportedCultures,
                //    FallBackToParentCultures = true,
                //    FallBackToParentUICultures = true
                //});
                var rlo = new RequestLocalizationOptions
                {
                    FallBackToParentCultures = true,
                    FallBackToParentUICultures = true
                };
                rlo.AddSupportedCultures(settings.Localization.SupportedCultures);
                rlo.AddSupportedUICultures(settings.Localization.SupportedCultures);
                rlo.SetDefaultCulture(settings.Localization.DefaultCulture);

                app.UseRequestLocalization(rlo);
            }

            // https://blog.maximerouiller.com/post/using-static-content-generation-in-asp.net-core/
            if (settings.PublicFileServer.Enable)
            {
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), settings.PublicFileServer.PhysicalPath)),
                    RequestPath = settings.PublicFileServer.RequestPath,
                    ContentTypeProvider = new FileExtensionContentTypeProvider(settings.PublicFileServer.ContentTypeMapping),
                    OnPrepareResponse = (context) =>
                    {
                        // custom headers from configuration
                        foreach (var header in settings.PublicFileServer.Headers)
                        {
                            context.Context.Response.Headers[header.Key] = header.Value;
                        }

                        // custom headers from SecurityHeadersMiddleware
                        foreach (var header in SecurityHeadersMiddleware.Headers)
                        {
                            context.Context.Response.Headers[header.Key] = header.Value;
                        }
                    }
                });

                if (settings.PublicFileServer.EnableDirectoryBrowsing)
                {
                    app.UseDirectoryBrowser(new DirectoryBrowserOptions
                    {
                        FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), settings.PublicFileServer.PhysicalPath)),
                        RequestPath = settings.PublicFileServer.RequestPath
                    });
                }
            }

            // TODO: Middleware: Change HttpStatusCode always 200 Ok

            // TODO: Middleware: Analytics/Metrics [GoogleAnalytics] [AppMetrics] [Prometheus]
            // TODO: Middleware: Centeralize Logging by level [ELK] [Text]

            app.UseSecurityHeaders();

            if (settings.Logging.Diagnostic.Enable)
                app.UseDiagnosticLog();

            app.UseErrorHandling();

            // ResponseWrappers for uncache & authentication
            if (settings.ResponseWrapper.Enable)
                app.UseResponseWrapper();

            if (settings.ConcurrentRequestsLimit.Enable)
                app.UseConcurrentRequestsLimit();

            // Enrichment - eg: Enrich with ClientIP in Header
            if (settings.Enrichment.Plugins != null && settings.Enrichment.Plugins.Any())
                app.UseEnrichment();

            // IP Restriction Blacklist/Whitelist on Endpoint and IP based
            // deny all, allow only whitelist or allow all, deny only blacklist
            if (settings.IPRestriciton.Enable)
                app.UseIPRestriciton();

            if (settings.AppAuthentication.Enable)
                app.UseAppAuthentication();

            if (settings.UserAuthentication.Enable)
                app.UseUserAuthentication();
            
            // RateLimit base on IP, Client(User) - [AspNetCoreRateLimit]
            if (settings.RateLimit.Enable)
            {
                if (settings.RateLimit.IpRateLimiting.Enable)
                    app.UseIpRateLimiting();
                if (settings.RateLimit.ClientRateLimiting.Enable)
                    app.UseClientRateLimiting();
            }

            if (settings.ResponseCache.Enable)
                app.UseResponseCache();

            // ResponseWrappers for success response, before cache
            if (settings.ResponseWrapper.Enable)
                app.UseResponseWrapper();

            // MicroserviceMiddleware - MicroservicePlugin (eg. MandatoryUpdate, ClientConfiguration)
            if (settings.Microservices.Any(x => x.Enable))
                app.UseMicroservices();

            // File Server
            if (settings.PrivateFileServer.Enable)
                app.UsePrivateFileServer();

            app.UseRoutingEngine(settings.RoutingEngine);
        }
    }
}
