﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class MetricsMonitoringSettings
    {
        public bool Enable { get; set; } = false;

        public int Port { get; set; } = 58348;
        public string EnvironmentInfoEndpoint { get; set; } = "/e";
        public string MetricsPrometheusEndpoint { get; set; } = "/m";
        public string MetricsJSONEndpoint { get; set; } = "/mj";
        public double ApdexTSeconds { get; set; } = 0;
        public IList<int> IgnoredHttpStatusCodes { get; set; }
        public IList<string> IgnoredRoutesRegexPatterns { get; set; } = new List<string>();
        public ReportToSettings ReportTo { get; set; } = new ReportToSettings();

        public class ReportToSettings
        {
            public bool Enable { get; set; } = false;

            public ConsoleSettings Console { get; set; }
            public TextFileSettings TextFile { get; set; }

            public class ConsoleSettings
            {
                public TimeSpan FlushInterval { get; set; }
            }
            public class TextFileSettings
            {
                public TimeSpan FlushInterval { get; set; }
                public bool Append { get; set; } = false;
                public string FilePath { get; set; }
            }
        }
    }
}
