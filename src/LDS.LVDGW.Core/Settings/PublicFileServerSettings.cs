﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    /// https://www.sitepoint.com/mime-types-complete-list/

    public class PublicFileServerSettings
    {
        public bool Enable { get; set; } = false;
        public string RequestPath { get; set; }
        public string PhysicalPath { get; set; } = "wwwroot";
        public bool EnableDirectoryBrowsing { get; set; } = false;
        public Dictionary<string, string> ContentTypeMapping { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();
    }
}
