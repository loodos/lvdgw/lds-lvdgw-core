﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class ResponseWrapperSettings
    {
        public bool Enable { get; set; } = false;
        public string ContentType { get; set; }
        public bool ForceHttpStatus200OK { get; set; } = false;
        public string Plugin { get; set; }
    }
}
