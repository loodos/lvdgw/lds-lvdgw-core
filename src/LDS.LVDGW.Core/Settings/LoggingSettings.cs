﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class LoggingSettings
    {
        public DiagnosticSettings Diagnostic { get; set; } = new DiagnosticSettings();

        public class DiagnosticSettings
        {
            public bool Enable { get; set; } = false;
            public string RealIpHeader { get; set; }
            public List<string> ContentTypesForResponseBodyLogging { get; set; } = new List<string>();
        }
    }
}
