﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class UserAuthenticationSettings
    {
        public bool Enable { get; set; } = false;

        public TokenSettings Token { get; set; } = new TokenSettings();
        public bool EnrichHttpContextUser { get; set; }
        public EnrichRequestSettings EnrichRequest { get; set; }
        public List<EndpointRuleSettings> EndpointRules { get; set; } = new List<EndpointRuleSettings>();
        public string Plugin { get; set; }

        public class TokenSettings
        {
            public AppSettings.ContainerType Container { get; set; } = AppSettings.ContainerType.Header;
            public string Key { get; set; }
        }
        public class EnrichRequestSettings
        {
            public AppSettings.ContainerType Container { get; set; } = AppSettings.ContainerType.Header;
            public string ContentType { get; set; } = "application/x-www-form-urlencoded";
            public string Prefix { get; set; }
        }
        public class EndpointRuleSettings
        {
            public string Endpoint { get; set; }
            public bool ValidationOptional { get; set; }
        }

    }
}
