﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class ConcurrentRequestsLimitSettings
    {
        public enum LimitExceededPolicies
        {
            /// <summary>
            /// DROP: if the application is processing maximum number of concurrent request 
            /// every incoming request should immediately result in 503 Service Unavailable.
            /// </summary>
            Drop,
            /// <summary>
            /// TAIL: if the application is processing maximum number of concurrent request 
            /// every incoming request wait in queue for resources to be available.
            /// (typically a FIFO one)
            /// </summary>
            FifoQueueDropTail,
            /// <summary>
            /// HEAD: when a new requests arrives (and the queue is full) 
            /// the first request in the queue is being dropped.
            /// (better latency for waiting requests)
            /// </summary>
            FifoQueueDropHead
        }

        public const int UnlimitedConcurrentRequests = -1;
        public const int UnlimitedQueueTimeout = -1;

        private int _limit;
        private int _queueSize;
        private int _queueTimeout;

        public bool Enable { get; set; } = false;
        public int Limit
        {
            get { return _limit; }
            set { _limit = (value < UnlimitedConcurrentRequests) ? UnlimitedConcurrentRequests : value; }
        }
        public LimitExceededPolicies LimitExceededPolicy { get; set; }
        public int QueueSize
        {
            get { return _queueSize; }
            set { _queueSize = (value < 0) ? 0 : value; }
        }
        public int QueueTimeoutInSeconds
        {
            get { return _queueTimeout / 1000; }

            set { _queueTimeout = (value <= 0) ? UnlimitedQueueTimeout : value * 1000; }
        }
    }
}
