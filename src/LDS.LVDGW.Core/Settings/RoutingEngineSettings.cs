﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class RoutingEngineSettings
    {
        public int TimeoutInSeconds { get; set; } = 20;
        public List<Route> Routes { get; set; } = new List<Route>();

        public class Route
        {
            public bool Enable { get; set; } = false;
            public string Name { get; set; }
            public string Template { get; set; }
            public string Destination { get; set; }
        }
    }
}
