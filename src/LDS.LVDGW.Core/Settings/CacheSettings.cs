﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class CacheSettings
    {
        public enum CacheStores
        {
            MemoryCache,
            DistributedRedisCache,
            DistributedSqlServerCache
        }

        public CacheDetailSettings MemoryCache { get; set; } = new CacheDetailSettings();
        public CacheDetailSettings DistributedRedisCache { get; set; } = new CacheDetailSettings();
        public CacheDetailSettings DistributedSqlServerCache { get; set; } = new CacheDetailSettings();

        public class CacheDetailSettings
        {
            public bool Enable { get; set; } = false;
            public Dictionary<string, object> Options { get; set; } = new Dictionary<string, object>();
        }
    }
}
