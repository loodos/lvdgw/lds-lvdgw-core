﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class IPRestricitonSettings
    {
        public bool Enable { get; set; } = false;
        public string ReadIPFromHeader { get; set; }
        public int HttpStatusCode { get; set; }
        public string RestrictionMessage { get; set; }
        public List<EndpointPolicySettings> EndpointPolicies { get; set; }
        public List<IPPolicySettings> IPPolicies { get; set; }

        public class EndpointPolicySettings
        {
            public string Endpoint { get; set; }
            // Deny all IPs expect Whitelist
            public string[] Whitelist { get; set; }
            // Allow all IPs expect Blacklist
            public string[] Blacklist { get; set; }
        }
        public class IPPolicySettings
        {
            public string IP { get; set; }
            // Deny all Endpoint expect Whitelist
            public string[] Whitelist { get; set; }
            // Allow all Endpoint expect Blacklist
            public string[] Blacklist { get; set; }
        }
    }
}
