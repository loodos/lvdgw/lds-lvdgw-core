﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class MicroserviceSettings
    {
        public bool Enable { get; set; } = false;
        public string Endpoint { get; set; }
        public string Plugin { get; set; }
    }
}
