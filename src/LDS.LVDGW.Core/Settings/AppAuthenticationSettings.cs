﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class AppAuthenticationSettings
    {
        public bool Enable { get; set; } = false;

        public AppIdSettings AppId { get; set; } = new AppIdSettings();
        public AppKeySettings AppKey { get; set; } = new AppKeySettings();
        public List<AppsSettings> Apps { get; set; } = new List<AppsSettings>();

        public class AppIdSettings
        {
            public string HeaderKey { get; set; }
            public string FormKey { get; set; }
            public string QueryKey { get; set; }
        }

        public class AppKeySettings
        {
            public string HeaderKey { get; set; }
            public string FormKey { get; set; }
            public string QueryKey { get; set; }
        }

        public class AppsSettings
        {
            public bool Enable { get; set; } = false;
            public string AppId { get; set; }
            public string AppKey { get; set; }
            public string[] AcceptedEndpoints { get; set; }
        }
    }
}
