﻿using AspNetCoreRateLimit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class RateLimitSettings
    {
        public bool Enable { get; set; } = false;

        public CacheSettings.CacheStores CacheStore { get; set; } = CacheSettings.CacheStores.MemoryCache;
        public IpRateLimitingSettings IpRateLimiting { get; set; } = new IpRateLimitingSettings();
        public ClientRateLimitingSettings ClientRateLimiting { get; set; } = new ClientRateLimitingSettings();

        public class IpRateLimitingSettings
        {
            public bool Enable { get; set; } = false;
            public IpRateLimitOptions Options { get; set; }
            public IpRateLimitPolicies Policies { get; set; }
        }

        public class ClientRateLimitingSettings
        {
            public bool Enable { get; set; } = false;
            public ClientRateLimitOptions Options { get; set; }
            public ClientRateLimitPolicies Policies { get; set; }
        }
    }
}
