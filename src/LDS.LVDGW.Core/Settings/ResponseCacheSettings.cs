﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class ResponseCacheSettings
    {
        public bool Enable { get; set; }
        public CacheSettings.CacheStores CacheStore { get; set; } = CacheSettings.CacheStores.MemoryCache;
        public string[] AcceptedVerbs { get; set; }
        public string[] AcceptedHeadersForHashKey { get; set; }
        public int DefaultExpiryInSeconds { get; set; } = 600;
        public List<EndpointRuleSettings> EndpointRules { get; set; }
        public Dictionary<string, string> ExcludeHeaders { get; set; }

        public class EndpointRuleSettings
        {
            public string Endpoint { get; set; }
            public int ExpiryInSeconds { get; set; }
            public bool NoCache { get; set; } = false;
        }
    }
}
