﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace LDS.LVDGW.Core.Settings
{
    /// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/index?view=aspnetcore-2.1&tabs=basicconfiguration#bind-to-an-object-graph
    /// https://odetocode.com/blogs/scott/archive/2017/04/24/asp-net-configuration-options-will-understand-arrays.aspx

    public class AppSettings
    {
        public enum ContainerType
        {
            Header,
            Form,
            Query
        }

        public List<PluginSettings> Plugins { get; set; } = new List<PluginSettings>();
        public CacheSettings Cache { get; set; } = new CacheSettings();
        public RoutingEngineSettings RoutingEngine { get; set; } = new RoutingEngineSettings();
        public ResponseCacheSettings ResponseCache { get; set; } = new ResponseCacheSettings();
        public ResponseWrapperSettings ResponseWrapper { get; set; } = new ResponseWrapperSettings();
        public EnrichmentSettings Enrichment { get; set; } = new EnrichmentSettings();
        public List<MicroserviceSettings> Microservices { get; set; } = new List<MicroserviceSettings>();
        public IPRestricitonSettings IPRestriciton { get; set; } = new IPRestricitonSettings();
        public AppAuthenticationSettings AppAuthentication { get; set; } = new AppAuthenticationSettings();
        public UserAuthenticationSettings UserAuthentication { get; set; } = new UserAuthenticationSettings();
        public ConcurrentRequestsLimitSettings ConcurrentRequestsLimit { get; set; } = new ConcurrentRequestsLimitSettings();
        public RateLimitSettings RateLimit { get; set; } = new RateLimitSettings();
        public LoggingSettings Logging { get; set; } = new LoggingSettings();
        public PublicFileServerSettings PublicFileServer { get; set; } = new PublicFileServerSettings();
        public PrivateFileServerSettings PrivateFileServer { get; set; } = new PrivateFileServerSettings();
        public MetricsMonitoringSettings MetricsMonitoring { get; set; } = new MetricsMonitoringSettings();
        public LocalizationSettings Localization { get; set; } = new LocalizationSettings();
        
    }

}
