﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class LocalizationSettings
    {
        public bool Enable { get; set; } = false;

        public string[] SupportedCultures { get; set; } = new string[] { "en-US" };
        public string DefaultCulture { get; set; } = "en-US";
        public string JsonResourcesPath { get; set; } = "json-resources";
    }
}
