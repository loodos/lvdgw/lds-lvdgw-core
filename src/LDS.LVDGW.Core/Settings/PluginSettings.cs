﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Settings
{
    public class PluginSettings
    {
        public string Name { get; set; }
        //public string Path { get; set; }
        public NugetSettings Nuget { get; set; }
        public Dictionary<string, object> Parameters { get; set; }

        public class NugetSettings
        {
            public string Name { get; set; }
            public string Version { get; set; }
            public string AssemblyName { get; set; }
            public string Source { get; set; }
        }
    }
}
