﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;

namespace LDS.LVDGW.Core.Services
{
    public interface ICacheService
    {
        bool IsDistributed { get; }

        Task<TItem> GetAsync<TItem>(string key) where TItem : class;
        Task SetAsync<TItem>(string key, TItem value, int expiryInSeconds = 0) where TItem : class;
    }

    public class MemoryCacheService : ICacheService
    {
        private IMemoryCache _cache;

        public MemoryCacheService(IMemoryCache cache)
        {
            _cache = cache;
        }

        public bool IsDistributed => false;

        public async Task<TItem> GetAsync<TItem>(string key) where TItem : class => await Task.Run(() =>
        {
            if (_cache.TryGetValue<TItem>(key, out var value))
            {
                return value;
            }

            return null;
        });

        public async Task SetAsync<TItem>(string key, TItem value, int expiryInSeconds = 0) where TItem : class => await Task.Run(() =>
        {
            _cache.Set<TItem>(key, value, TimeSpan.FromSeconds(expiryInSeconds));
        });
        
    }

    public class DistributedCacheService : ICacheService
    {
        private IDistributedCache _cache;

        public DistributedCacheService(IDistributedCache cache)
        {
            _cache = cache;
        }

        public bool IsDistributed => true;

        public async Task<TItem> GetAsync<TItem>(string key) where TItem : class
        {
            var bytes = await _cache.GetAsync(key);

            if (bytes == null)
                return null;

            using (var stream = new MemoryStream(bytes))
            {
                var value = new BinaryFormatter().Deserialize(stream) as TItem;

                return value;
            }
        }

        public async Task SetAsync<TItem>(string key, TItem value, int expiryInSeconds = 0) where TItem : class
        {
            using (var stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, value);
                var bytes = stream.ToArray();

                var cacheEntryOptions = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromSeconds(expiryInSeconds));

                await _cache.SetAsync(key, bytes, cacheEntryOptions);
            }
        }
        
    }
}
