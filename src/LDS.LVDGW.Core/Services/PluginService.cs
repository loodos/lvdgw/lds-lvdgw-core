﻿using LDS.LVDGW.Core.Extensibility;
using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Plugin;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Services
{
    public class PluginService
    {
        private const string PLUGINS_ASSEMBLY_FORMAT = "LDS.LVDGW.Plugin.*.dll";

        private readonly List<PluginSettings> _pluginSettings;
        private readonly IServiceCollection _services;
        private IDictionary<string, IPlugin> _pluginInstances = new Dictionary<string, IPlugin>();
        private List<string> _pluginAddedAsService = new List<string>();

        public PluginService(IServiceCollection services, List<PluginSettings> pluginSettings)
        {
            _pluginSettings = pluginSettings;
            _services = services;

            LoadAllPlugins();
        }

        public void AddPluginService<TService>(string pluginName) where TService : class
        {
            if (string.IsNullOrEmpty(pluginName))
                throw new ArgumentNullException(pluginName);

            if (!_pluginAddedAsService.Contains(pluginName))
            {
                var plugin = (TService)GetByName(pluginName);

                _services.AddSingleton<TService>(plugin);
                _pluginAddedAsService.Add(pluginName);
            }
        }

        private IPlugin GetByName(string name)
        {
            if (!_pluginInstances.ContainsKey(name))
                throw new KeyNotFoundException($"Plugin not found: {name}. Check defined plugin names");

            return _pluginInstances[name];
        }

        private void LoadAllPlugins()
        {
            foreach (var plugin in _pluginSettings)
            {
                if (string.IsNullOrEmpty(plugin.Name))
                    throw new ArgumentNullException("Plugins.Name");
                
                if (_pluginInstances.ContainsKey(plugin.Name))
                    throw new IndexOutOfRangeException($"Plugin[{plugin.Name}] already defined. Plugin name must be unique.");

                if (plugin.Nuget == null)
                    throw new ArgumentNullException("Plugins.Nuget");

                if (string.IsNullOrEmpty(plugin.Nuget.Name))
                    throw new ArgumentNullException("Plugins.Nuget.Name");

                if (string.IsNullOrEmpty(plugin.Nuget.Version))
                    throw new ArgumentNullException("Plugins.Nuget.Version");

                if (string.IsNullOrEmpty(plugin.Nuget.AssemblyName))
                    throw new ArgumentNullException("Plugins.Nuget.AssemblyName");

                if (string.IsNullOrEmpty(plugin.Nuget.Source))
                    throw new ArgumentNullException("Plugins.Nuget.Source");


                Assembly assembly = Assembly.Load(new AssemblyName(plugin.Nuget.AssemblyName));

                //Find any IPlugin interface implementation in assembly then create an instance
                assembly.GetTypes().Where(x => x.GetInterfaces().Any(y => y == typeof(IPlugin)))
                    .ToList().ForEach(pluginType =>
                    {
                        var pluginInstance = (IPlugin)Activator.CreateInstance(pluginType);

                        var Log = Serilog.Log.ForContext(pluginInstance.GetType());

                        Log.Information("'{PluginName}' Plugin initialized.", plugin.Name);

                        pluginInstance.InstanceName = plugin.Name;
                        pluginInstance.Log = Log;
                        pluginInstance.SetAndValidateParameters(plugin.Parameters);
                        pluginInstance.ConfigureServices(_services);

                        _pluginInstances.Add(plugin.Name, pluginInstance);
                    });
            }
        }
    }
}
