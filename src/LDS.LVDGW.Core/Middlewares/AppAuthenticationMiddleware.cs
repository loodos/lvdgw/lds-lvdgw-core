﻿using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Core.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    public class AppAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppAuthenticationSettings _settings;

        private static readonly ILogger Log = Serilog.Log.ForContext<AppAuthenticationMiddleware>();

        public AppAuthenticationMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings?.Value?.AppAuthentication ?? throw new ArgumentNullException(nameof(AppAuthenticationSettings));
        }

        public async Task Invoke_old(HttpContext httpContext)
        {
            var request = httpContext.Request;
            var response = httpContext.Response;

            var appId = GetAppId(request);
            var appKey = GetAppKey(request);

            if (!string.IsNullOrWhiteSpace(appId) && !string.IsNullOrWhiteSpace(appKey))
            {
                var app = _settings.Apps.FirstOrDefault(x => x.AppId == appId && x.AppKey == appKey && x.Enable);

                if (app != null)
                {
                    if (IsAcceptedPath(request, app))
                    {
                        await _next(httpContext);
                    }
                    else
                    {
                        await HandleAuthenticationFailAsync(response, "Method is unauthorized for this application.");
                    }
                }
                else
                {
                    await HandleAuthenticationFailAsync(response, "AppId and/or AppKey not valid.");
                }
            }
            else
            {
                await HandleAuthenticationFailAsync(response, "AppId and/or AppKey not found.");
            }
        }
        public async Task Invoke(HttpContext httpContext)
        {
            var request = httpContext.Request;
            var response = httpContext.Response;

            var appId = GetAppId(request);
            var appKey = GetAppKey(request);

            var apps = _settings.Apps.Where(x => x.Enable && IsAcceptedPath(request, x)).ToList();

            if (apps != null && apps.Count > 0)
            {
                foreach (var app in apps)
                {
                    if (app.AppId == appId && app.AppKey == appKey)
                    {
                        await _next(httpContext);
                        return;
                    }
                }
                await HandleAuthenticationFailAsync(response, "AppId and/or AppKey not valid.");
            }
            else
            {
                await _next(httpContext);
            }
        }

        private string GetAppId(HttpRequest request)
        {
            var values = StringValues.Empty;

            if (!string.IsNullOrEmpty(_settings.AppId.HeaderKey))
            {
                if (request.Headers.TryGetValue(_settings.AppId.HeaderKey, out values))
                {
                    return values.FirstOrDefault();
                }
            }

            if (!string.IsNullOrEmpty(_settings.AppId.FormKey))
            {
                if (request.HasFormContentType && request.Form.TryGetValue(_settings.AppId.FormKey, out values))
                {
                    return values.FirstOrDefault();
                }
            }

            if (!string.IsNullOrEmpty(_settings.AppId.QueryKey))
            {
                if (request.Query.TryGetValue(_settings.AppId.QueryKey, out values))
                {
                    return values.FirstOrDefault();
                }
            }

            return null;
        }

        private string GetAppKey(HttpRequest request)
        {
            var values = StringValues.Empty;

            if (!string.IsNullOrEmpty(_settings.AppKey.HeaderKey))
            {
                if (request.Headers.TryGetValue(_settings.AppKey.HeaderKey, out values))
                {
                    return values.FirstOrDefault();
                }
            }

            if (!string.IsNullOrEmpty(_settings.AppKey.FormKey))
            {
                if (request.HasFormContentType && request.Form.TryGetValue(_settings.AppKey.FormKey, out values))
                {
                    return values.FirstOrDefault();
                }

            }

            if (!string.IsNullOrEmpty(_settings.AppKey.QueryKey))
            {
                if (request.Query.TryGetValue(_settings.AppKey.QueryKey, out values))
                {
                    return values.FirstOrDefault();
                }
            }

            return null;
        }

        private bool IsAcceptedPath(HttpRequest request, AppAuthenticationSettings.AppsSettings app)
        {
            if (app.AcceptedEndpoints != null && app.AcceptedEndpoints.Any())
            {
                foreach (var endpointString in app.AcceptedEndpoints)
                {
                    var endpoint = Endpoint.Parse(endpointString);
                    if (endpoint.IsMatch(request.Path.Value, request.Method))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private async Task HandleAuthenticationFailAsync(HttpResponse response, string message)
        {
            Log.Debug(message);

            response.StatusCode = StatusCodes.Status401Unauthorized;
            response.ContentType = "text/plain";
            await response.WriteAsync(message);
        }
    }
}
