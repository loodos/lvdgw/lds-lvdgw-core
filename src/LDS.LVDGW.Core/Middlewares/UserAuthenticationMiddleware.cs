﻿using LDS.LVDGW.Core.Extensions;
using LDS.LVDGW.Core.Helpers;
using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Core.Tools;
using LDS.LVDGW.Plugin.UserAuthentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Serilog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    public class UserAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly UserAuthenticationSettings _settings;
        private readonly IUserAuthenticationPlugin _userAuthenticationPlugin;

        private static readonly ILogger Log = Serilog.Log.ForContext<UserAuthenticationMiddleware>();

        public UserAuthenticationMiddleware(RequestDelegate next,
            IOptions<AppSettings> appSettings,
            IUserAuthenticationPlugin userAuthenticationPlugin)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings.Value?.UserAuthentication ?? throw new ArgumentNullException(nameof(UserAuthenticationSettings));
            _userAuthenticationPlugin = userAuthenticationPlugin;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var request = httpContext.Request;
            var response = httpContext.Response;

            if (_settings.Enable)
            {
                var endpointSettings = GetValidateEndpointSettings(request);

                if (endpointSettings != null)
                {
                    var token = GetToken(request);

                    // token not found
                    if (string.IsNullOrWhiteSpace(token))
                    {
                        // token required
                        if (!endpointSettings.ValidationOptional)
                        {
                            await HandleAuthenticationFailAsync(response, "Token not found.");
                        }
                        // token optional
                        else
                        {
                            await _next(httpContext);
                        }
                    }
                    // token found, decode&validate it
                    else
                    {
                        try
                        {
                            var claims = await _userAuthenticationPlugin.ValidateAndRetrieveClaimsAsync(token);

                            EnrichRequestWithClaims(request, claims);
                            EnrichHttpContextUser(request, claims);
                            await _next(httpContext);
                        }
                        catch (TokenExpiredException ex)
                        {
                            await HandleAuthenticationFailAsync(response, ex.Message, StatusCodes.Status419AuthenticationTimeout);

                        }
                        catch (InvalidTokenException ex)
                        {
                            await HandleAuthenticationFailAsync(response, ex.Message);
                        }
                        catch (Exception ex)
                        {
                            await HandleAuthenticationFailAsync(response, ex.Message);
                        }
                    }
                }
                else
                {
                    await _next(httpContext);
                }
            }
            else
            {
                await _next(httpContext);
            }
        }

        private UserAuthenticationSettings.EndpointRuleSettings GetValidateEndpointSettings(HttpRequest request)
        {
            foreach (var endpointRule in _settings.EndpointRules)
            {
                var endpoint = Endpoint.Parse(endpointRule.Endpoint);

                if (endpoint.IsMatch(request.Path.Value, request.Method))
                {
                    return endpointRule;
                }
            }

            return null;
        }

        private string GetToken(HttpRequest request)
        {
            var values = StringValues.Empty;

            if (_settings.Token != null)
            {
                var container = _settings.Token.Container;
                var key = _settings.Token.Key;

                if (container == AppSettings.ContainerType.Header)
                {
                    request.Headers.TryGetValue(key, out values);
                }
                else if (container == AppSettings.ContainerType.Form && request.HasFormContentType)
                {
                    request.Form.TryGetValue(key, out values);
                }
                else if (container == AppSettings.ContainerType.Query)
                {
                    request.Query.TryGetValue(key, out values);
                }
            }

            return values.FirstOrDefault();
        }

        private void EnrichRequestWithClaims(HttpRequest request, IDictionary<string, object> claims)
        {
            if (_settings.EnrichRequest != null)
            {
                var container = _settings.EnrichRequest.Container;
                var prefix = _settings.EnrichRequest.Prefix;

                if (container == AppSettings.ContainerType.Header)
                {
                    claims.Keys.ToList().ForEach(key =>
                    {
                        // "Request headers must contain only ASCII characters"
                        request.Headers.Add($"{prefix}{key}", new StringValues(HttpUtility.UrlEncode(claims[key].ToString())));
                    });
                }
                else if (container == AppSettings.ContainerType.Form)
                {
                    var files = request.Form.Files;
                    if (string.IsNullOrEmpty(request.ContentType))
                    {
                        request.ContentType = _settings.EnrichRequest.ContentType;
                    }
                    var collection = request.Form.AsNameValueCollection();

                    claims.Keys.ToList().ForEach(key =>
                    {
                        collection.Add($"{prefix}{key}", claims[key].ToString());
                    });

                    request.Form = new FormCollection(collection.AsDictionary(),files);
                }
                else if (container == AppSettings.ContainerType.Query)
                {
                    var collection = request.Query.AsNameValueCollection();

                    claims.Keys.ToList().ForEach(key =>
                    {
                        collection.Add($"{prefix}{key}", claims[key].ToString());
                    });

                    request.Query = new QueryCollection(collection.AsDictionary());
                }
            }
        }

        private void EnrichHttpContextUser(HttpRequest request, IDictionary<string, object> claims)
        {
            if (_settings.EnrichHttpContextUser)
            {
                Log.Information("[C_UAM1] Enriching HttpContext User");
                var claimList = new List<Claim>();

                string userName = "";
                foreach (var c in claims)
                {
                    if (c.Key == "userName")
                    {
                        userName = c.Value.ToString();
                    }

                    claimList.Add(new Claim(c.Key, c.Value.ToString()));
                }
                if (string.IsNullOrEmpty(userName) == false)
                {
                    var identity = new ClaimsIdentity(
                        new GenericIdentity(userName, "TokenAuth"),
                        claimList.ToArray()
                    );
                    var user = new GenericPrincipal(identity, new string[] { });

                    request.HttpContext.User = user;
                }
                else
                {
                    Log.Information("[C_UAM2] UserName claim not found");
                }
            }
        }

        private async Task HandleAuthenticationFailAsync(HttpResponse response, string message, int statusCode = StatusCodes.Status401Unauthorized)
        {
            Log.Debug(message);

            response.StatusCode = statusCode;
            response.ContentType = "text/plain";
            await response.WriteAsync(message);
        }
    }
}
