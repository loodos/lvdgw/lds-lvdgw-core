﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using LDS.LVDGW.Plugin.Enrichment;
using LDS.LVDGW.Core.Settings;
using Serilog;

namespace LDS.LVDGW.Core.Middlewares
{
    public class EnrichmentMiddleware
    {
        private readonly RequestDelegate _next;

        private static readonly ILogger Log = Serilog.Log.ForContext<EnrichmentMiddleware>();

        public EnrichmentMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var enrichmentPlugins = httpContext.RequestServices.GetServices<IEnrichmentPlugin>();

            foreach (var enrichmentPlugin in enrichmentPlugins)
            {
                try
                {
                    Log.Debug("Enrich with '{PluginName}' enrichment plugin", enrichmentPlugin.InstanceName);

                    await enrichmentPlugin.EnrichAsync(httpContext);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Enrich with '{PluginName}' enrichment plugin has error!", enrichmentPlugin.InstanceName);
                }
            }

            await _next(httpContext);
        }
    }
}
