﻿using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Core.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    public class DiagnosticLogMiddleware
    {
        private const string MessageTemplate = "HTTP {RequestMethod} {RequestPath} responded {ResponseStatusCode} in {Elapsed:0.0000} ms";

        private readonly RequestDelegate _next;
        private readonly LoggingSettings.DiagnosticSettings _settings;

        private static readonly ILogger Log = Serilog.Log.ForContext<DiagnosticLogMiddleware>();

        public DiagnosticLogMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings.Value?.Logging?.Diagnostic ?? throw new ArgumentNullException(nameof(LoggingSettings.DiagnosticSettings));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var start = Stopwatch.GetTimestamp();

            // Shim Response Stream and cache response
            var originalBodyStream = httpContext.Response.Body;

            try
            {
                using(var responseBodyStream = new MemoryStream())
                {
                    //Replace stream for upstream calls.
                    httpContext.Response.Body = responseBodyStream;

                    await _next(httpContext);

                    string responseBody = null;
                    if (httpContext.Response.ContentType != null && _settings.ContentTypesForResponseBodyLogging.Any(x => httpContext.Response.ContentType.Contains(x)))
                    {
                        httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
                        responseBody = await new StreamReader(httpContext.Response.Body).ReadToEndAsync();
                    }

                    LogDiagnostic(httpContext, responseBody, GetElapsedMilliseconds(start, Stopwatch.GetTimestamp()));

                    // Unshim Response Stream
                    httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
                    await responseBodyStream.CopyToAsync(originalBodyStream);
                }
            }
            finally
            {
                //and finally, reset the stream for downstream calls
                httpContext.Response.Body = originalBodyStream;
            }
        }

        private void LogDiagnostic(HttpContext httpContext, string responseBody, double elapsedMs)
        {
            var request = httpContext.Request;
            var response = httpContext.Response;

            var responseStatusCode = response?.StatusCode;
            var level = responseStatusCode > 499 ? LogEventLevel.Error : LogEventLevel.Information;

            var logger = Log
                .ForContext("RequestHeaders", request.Headers.ToDictionary(h => h.Key, h => h.Value.ToString()), destructureObjects: true)
                .ForContext("RequestHost", request.Host)
                .ForContext("RequestProtocol", request.Protocol)
                .ForContext("IP", IPUtil.GetClientIp(httpContext, _settings.RealIpHeader))
                .ForContext("ResponseHeaders", response.Headers.ToDictionary(h => h.Key, h => h.Value.ToString()), destructureObjects: true)
                .ForContext("ResponseBody", responseBody);

            if (request.HasFormContentType)
                logger = logger.ForContext("RequestForm", request.Form.ToDictionary(v => v.Key, v => v.Value.ToString()));

            if (request.Query != null && request.Query.Any())
                logger = logger.ForContext("RequestQuery", request.Query.ToDictionary(v => v.Key, v => v.Value.ToString()));

            logger.Write(level, MessageTemplate, request.Method, request.Path, responseStatusCode, elapsedMs);
        }

        private double GetElapsedMilliseconds(long start, long stop)
        {
            return (stop - start) * 1000 / (double)Stopwatch.Frequency;
        }
    }
}
