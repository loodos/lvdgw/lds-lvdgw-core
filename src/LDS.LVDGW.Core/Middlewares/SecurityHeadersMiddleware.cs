﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    public class SecurityHeadersMiddleware
    {
        public readonly static Dictionary<string, string> Headers = new Dictionary<string, string> {
            { "X-Frame-Options", "DENY" },
            { "X-XSS-Protection", "1; mode=block" },
            { "X-Content-Type-Options", "nosniff" },
            { "Strict-Transport-Security", "max-age=31536000" }, //one year in seconds
            { "X-Powered-By", "L O O D O S" },
            { "Server", "Loodos Vanilla Delivery GateWay" }
        };

        private readonly RequestDelegate _next;

        public SecurityHeadersMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            httpContext.Response.OnStarting(() =>
            {
                foreach (var header in Headers)
                {
                    httpContext.Response.Headers[header.Key] = header.Value;
                }

                return Task.FromResult(0);
                //return Task.CompletedTask;
            });

            await _next(httpContext);
        }
    }
}
