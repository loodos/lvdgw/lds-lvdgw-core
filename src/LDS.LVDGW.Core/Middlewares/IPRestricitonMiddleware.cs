﻿using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Core.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    public class IPRestricitonMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IPRestricitonSettings _settings;

        private static readonly ILogger Log = Serilog.Log.ForContext<IPRestricitonMiddleware>();

        public IPRestricitonMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings.Value?.IPRestriciton ?? throw new ArgumentNullException(nameof(IPRestricitonSettings));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var request = httpContext.Request;

            var clientIP = IPUtil.GetClientIp(httpContext, _settings.ReadIPFromHeader);

            if (CheckEndpointPolicies(request, clientIP) || CheckIPPolicies(request, clientIP))
            {
                // send deny response
                var message = string.Format(_settings.RestrictionMessage, clientIP.ToString());

                Log.Debug(message);

                httpContext.Response.StatusCode = _settings.HttpStatusCode;
                httpContext.Response.ContentType = "text/plain";
                await httpContext.Response.WriteAsync(message);
            }
            else
            {
                await _next(httpContext);
            }
        }

        private bool CheckEndpointPolicies(HttpRequest request, IPAddress clientIP)
        {
            if (_settings.EndpointPolicies != null && _settings.EndpointPolicies.Any())
            {
                foreach (var endpointPolicy in _settings.EndpointPolicies)
                {
                    var endpoint = Endpoint.Parse(endpointPolicy.Endpoint);

                    if (endpoint.IsMatch(request.Path.Value, request.Method))
                    {
                        // Allow all IPs expect Blacklist
                        if (endpointPolicy.Blacklist != null)
                        {
                            if (IPUtil.ContainsIp(endpointPolicy.Blacklist, clientIP))
                            {
                                return true;
                            }
                        }
                        // Deny all IPs expect Whitelist
                        else // if (endpointPolicy.Whitelist != null)
                        {
                            if (!IPUtil.ContainsIp(endpointPolicy.Whitelist, clientIP))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool CheckIPPolicies(HttpRequest request, IPAddress clientIP)
        {
            if (_settings.IPPolicies != null && _settings.IPPolicies.Any())
            {
                foreach (var IPPolicy in _settings.IPPolicies)
                {
                    if (IPUtil.ContainsIp(IPPolicy.IP, clientIP))
                    {
                        // Allow all Endpoint expect Blacklist
                        if (IPPolicy.Blacklist != null)
                        {
                            foreach (var endpointString in IPPolicy.Blacklist)
                            {
                                var endpoint = Endpoint.Parse(endpointString);

                                if (endpoint.IsMatch(request.Path.Value, request.Method))
                                {
                                    return true;
                                }
                            }
                        }
                        // Deny all Endpoint expect Whitelist
                        else // if (IPPolicy.Whitelist != null)
                        {
                            var denyRequest = true;
                            foreach (var endpointString in IPPolicy.Whitelist)
                            {
                                var endpoint = Endpoint.Parse(endpointString);

                                if (endpoint.IsMatch(request.Path.Value, request.Method))
                                {
                                    denyRequest = false;
                                    break;
                                }
                            }

                            return denyRequest;
                        }
                    }
                }
            }

            return false;
        }
    }
}
