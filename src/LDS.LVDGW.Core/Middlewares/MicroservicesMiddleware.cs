﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Plugin.Microservice;
using System.Text.RegularExpressions;
using System.IO;
using LDS.LVDGW.Core.Tools;
using Serilog;
using App.Metrics;
using App.Metrics.Timer;
using LDS.LVDGW.Core.Extensibility;

namespace LDS.LVDGW.Core.Middlewares
{
    public class MicroservicesMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly List<MicroserviceSettings> _settings;
        private readonly IMetrics _metrics;

        private static readonly ILogger Log = Serilog.Log.ForContext<MicroservicesMiddleware>();
        private static readonly TimerOptions TimerOptions = new TimerOptions
        {
            Context = "MicroservicesMiddleware",
            Name = "uniform",
        };

        public MicroservicesMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, IMetrics metrics)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings?.Value?.Microservices ?? throw new ArgumentNullException(nameof(List<MicroserviceSettings>));
            
            _metrics = appSettings.Value.MetricsMonitoring.Enable ? metrics : null;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (!await TryHandleMicroserviceResponse(httpContext))
            {
                await _next(httpContext);
            }
        }

        private async Task<bool> TryHandleMicroserviceResponse(HttpContext httpContext)
        {
            var microserviceResponse = await GetMicroserviceResponse(httpContext);

            if (microserviceResponse != null)
            {
                httpContext.Response.StatusCode = microserviceResponse.StatusCode;
                httpContext.Response.ContentType = microserviceResponse.ContentType;
                microserviceResponse.Headers?.ToList().ForEach(header => {
                    httpContext.Response.Headers[header.Key] = header.Value;
                });

                // write response from microservice
                await httpContext.Response.WriteAsync(microserviceResponse.Body);
                //using (var output = new MemoryStream(microserviceResponse.BodyBytes))
                //{
                //    await output.CopyToAsync(httpContext.Response.Body);
                //}

                return true;
            }

            return false;
        }

        private async Task<MicroserviceResponse> GetMicroserviceResponse(HttpContext httpContext)
        {
            var microservicePlugin = GetMatchedMicroservicePlugin(httpContext);

            if (microservicePlugin != null)
            {
                Log.Debug("Enrich with '{PluginName}' microservice plugin", microservicePlugin.InstanceName);

                using (_metrics?.Measure.Timer.Time(MetricsRegistry.PluginMetrics.TimerExecution, MetricTags.FromSetItemString(microservicePlugin.InstanceName)))
                {
                    var microserviceResponse = await microservicePlugin.ExecAsync(httpContext.Request);

                    if (microserviceResponse != null)
                    {
                        return microserviceResponse;
                    }
                }
            }

            return null;
        } 

        private IMicroservicePlugin GetMatchedMicroservicePlugin(HttpContext httpContext)
        {
            var matchedMicroServiceSettings = GetMatchedMicroserviceSettings(httpContext.Request.Method, httpContext.Request.Path.Value);

            if (matchedMicroServiceSettings != null)
            {
                var microservicePlugin = httpContext.RequestServices.GetServices<IMicroservicePlugin>().FirstOrDefault(x => x.InstanceName == matchedMicroServiceSettings.Plugin);

                if (microservicePlugin != null)
                {
                    return microservicePlugin;
                }
            }

            return null;
        }

        private MicroserviceSettings GetMatchedMicroserviceSettings(string verb, string path)
        {
            foreach (var microService in _settings.Where(x => x.Enable))
            {
                var endpoint = Endpoint.Parse(microService.Endpoint);

                if (endpoint.IsMatch(path, verb))
                {
                    return microService;
                }
            }

            return null;
        }
    }
}
