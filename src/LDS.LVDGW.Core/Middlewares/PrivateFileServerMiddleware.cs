﻿using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Core.Tools;
using LDS.LVDGW.Plugin;
using LDS.LVDGW.Plugin.ResponseWrapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    public class PrivateFileServerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly PrivateFileServerSettings _settings;

        private static readonly ILogger Log = Serilog.Log.ForContext<PrivateFileServerMiddleware>();

        public PrivateFileServerMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings.Value?.PrivateFileServer ?? throw new ArgumentNullException(nameof(PrivateFileServerSettings));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var request = httpContext.Request;
            var response = httpContext.Response;

            if (IsPrivateFileServerRequest(request.Path, out PathString remaining))
            {
                var fileName = remaining.Value.TrimStart('/');
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), _settings.PhysicalPath, fileName);

                if (File.Exists(filePath))
                {
                    response.ContentType = GetContentType(filePath);

                    if (!string.IsNullOrEmpty(response.ContentType))
                    {
                        response.StatusCode = StatusCodes.Status200OK;

                        using (var output = File.OpenRead(filePath))
                        {
                            await output.CopyToAsync(response.Body);
                        }
                    }
                    else
                    {
                        await HandleFailAsync(response, $"File not supported: {fileName}");
                    }
                }
                else
                {
                    await HandleFailAsync(response, $"File not found: {fileName}");
                }
            }
            else
            {
                await _next(httpContext);
            }
        }

        private bool IsPrivateFileServerRequest(PathString path, out PathString remaining)
        {
            return path.StartsWithSegments(new PathString(_settings.RequestPath), StringComparison.InvariantCultureIgnoreCase, out remaining);
        }

        private string GetContentType(string path)
        {
            var extension = Path.GetExtension(path);

            if (_settings.ContentTypeMapping.ContainsKey(extension))
                return _settings.ContentTypeMapping[extension];
            else
                return null;
        }

        private async Task HandleFailAsync(HttpResponse response, string message)
        {
            Log.Debug(message);

            response.StatusCode = StatusCodes.Status404NotFound;
            response.ContentType = "text/plain";
            await response.WriteAsync(message);
        }

    }
}
