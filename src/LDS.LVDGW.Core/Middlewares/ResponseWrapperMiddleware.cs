﻿using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Plugin;
using LDS.LVDGW.Plugin.ResponseWrapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    /// https://github.com/aspnet/ResponseCaching/blob/f8831928de92f7900f26b74f64eb4083227f257a/src/Microsoft.AspNetCore.ResponseCaching/ResponseCachingMiddleware.cs
    /// https://elanderson.net/2017/02/log-requests-and-responses-in-asp-net-core/
    /// https://stackoverflow.com/questions/47181356/c-sharp-dotnet-core-middleware-wrap-response

    public class ResponseWrapperMiddleware
    {
        private const string HTTPCONTEXT_ITEMS_ALREADY_PROCESSED_IN_PIPELINE = "HTTPCONTEXT_ITEMS_ALREADY_PROCESSED_IN_PIPELINE";

        private readonly RequestDelegate _next;
        private readonly ResponseWrapperSettings _settings;
        private readonly IResponseWrapperPlugin _responseWrapperPlugin;

        private static readonly ILogger Log = Serilog.Log.ForContext<ResponseWrapperMiddleware>();

        public ResponseWrapperMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, IResponseWrapperPlugin responseWrapperPlugin)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings.Value?.ResponseWrapper ?? throw new ArgumentNullException(nameof(ResponseWrapperSettings));
            _responseWrapperPlugin = responseWrapperPlugin;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            // Shim Response Stream and cache response
            var originalBodyStream = httpContext.Response.Body;

            try
            {
                using (var responseBodyStream = new MemoryStream())
                {
                    //Replace stream for upstream calls.
                    httpContext.Response.Body = responseBodyStream;

                    await _next(httpContext);

                    // if cached response or wrapped before DONT WRAP again!
                    if (httpContext.Items.ContainsKey(HTTPCONTEXT_ITEMS_ALREADY_PROCESSED_IN_PIPELINE)
                        || httpContext.Response.Headers.ContainsKey(ResponseCacheMiddleware.HTTPHEADER_CACHE_CREATED))
                    {

                        // Unshim Response Stream
                        httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
                        await responseBodyStream.CopyToAsync(originalBodyStream);
                    }
                    else
                    {
                        //reset position to read data stored in response stream
                        httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
                        string responseBody = await new StreamReader(httpContext.Response.Body).ReadToEndAsync();

                        //wrap response with plugin
                        var wrappedResponseBody = await _responseWrapperPlugin.WrapResponseAsync(responseBody, httpContext.Response.StatusCode, httpContext.Response.ContentType);

                        // responseBody was wrapped
                        if (wrappedResponseBody != responseBody)
                        {
                            Log.Debug("Response wrapped");

                            //change content type
                            httpContext.Response.ContentType = _settings.ContentType;

                            if (_settings.ForceHttpStatus200OK)
                            {
                                httpContext.Response.StatusCode = StatusCodes.Status200OK;
                            }

                            httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
                            
                            //convert json to a stream
                            var buffer = Encoding.UTF8.GetBytes(wrappedResponseBody);
                            using (var output = new MemoryStream(buffer))
                            {
                                // Unshim Response Stream
                                await output.CopyToAsync(originalBodyStream);
                            }
                        }
                        // same responseBody, dont change
                        else
                        {
                            httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
                            await responseBodyStream.CopyToAsync(originalBodyStream);
                        }
                        
                        // mark already processed
                        httpContext.Items.Add(HTTPCONTEXT_ITEMS_ALREADY_PROCESSED_IN_PIPELINE, true);
                    }
                }
            }
            finally
            {
                //and finally, reset the stream for downstream calls
                httpContext.Response.Body = originalBodyStream;
            }
        }

        

    }
}
