﻿using App.Metrics;
using LDS.LVDGW.Core.Extensibility;
using LDS.LVDGW.Core.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Middlewares
{
    /// https://www.tpeczek.com/2017/08/implementing-concurrent-requests-limit.html
    /// https://github.com/tpeczek/Demo.AspNetCore.MaxConcurrentRequests/tree/semaphoreslim-based-synchronization

    public class ConcurrentRequestsLimitMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ConcurrentRequestsLimitSettings _settings;
        private readonly ConcurrentRequestsEnqueuer _enqueuer;
        private readonly IMetrics _metrics;
        private readonly IStringLocalizer<ConcurrentRequestsLimitMiddleware> _localizer;

        private static readonly ILogger Log = Serilog.Log.ForContext<ConcurrentRequestsLimitMiddleware>();

        private int _concurrentRequestsCount;

        public ConcurrentRequestsLimitMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, IMetrics metrics, IStringLocalizer<ConcurrentRequestsLimitMiddleware> localizer)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings?.Value?.ConcurrentRequestsLimit ?? throw new ArgumentNullException(nameof(ConcurrentRequestsLimitSettings));

            _metrics = appSettings.Value.MetricsMonitoring.Enable ? metrics : null;
            _localizer = localizer ?? throw new ArgumentNullException(nameof(localizer));

            if (_settings.LimitExceededPolicy != ConcurrentRequestsLimitSettings.LimitExceededPolicies.Drop)
            {
                _enqueuer = new ConcurrentRequestsEnqueuer(_settings);
            }
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (CheckLimitExceeded() && !(await TryWaitInQueueAsync(httpContext.RequestAborted)))
            {
                if (!httpContext.RequestAborted.IsCancellationRequested)
                {
                    Log.Debug("Concurrent request limit exceeded.");

                    httpContext.Response.StatusCode = StatusCodes.Status503ServiceUnavailable;
                    httpContext.Response.ContentType = "text/plain";
                    await httpContext.Response.WriteAsync(_localizer["REQUEST_LIMIT_EXCEEDED"]);
                }
            }
            else
            {
                _metrics?.Measure.Gauge.SetValue(MetricsRegistry.Gauges.ConcurrentRequests, _concurrentRequestsCount);

                try
                {
                    await _next(httpContext);
                }
                finally
                {
                    if (await ShouldDecrementConcurrentRequestsCountAsync())
                    {
                        Interlocked.Decrement(ref _concurrentRequestsCount);
                    }

                    _metrics?.Measure.Gauge.SetValue(MetricsRegistry.Gauges.ConcurrentRequests, _concurrentRequestsCount);
                }
            }
        }

        private bool CheckLimitExceeded()
        {
            bool limitExceeded;

            if (_settings.Limit == ConcurrentRequestsLimitSettings.UnlimitedConcurrentRequests)
            {
                limitExceeded = false;
            }
            else
            {
                int initialConcurrentRequestsCount, incrementedConcurrentRequestsCount;
                do
                {
                    limitExceeded = true;

                    initialConcurrentRequestsCount = _concurrentRequestsCount;
                    if (initialConcurrentRequestsCount >= _settings.Limit)
                    {
                        break;
                    }

                    limitExceeded = false;
                    incrementedConcurrentRequestsCount = initialConcurrentRequestsCount + 1;
                }
                while (initialConcurrentRequestsCount != Interlocked.CompareExchange(ref _concurrentRequestsCount, incrementedConcurrentRequestsCount, initialConcurrentRequestsCount));
            }

            return limitExceeded;
        }

        private async Task<bool> TryWaitInQueueAsync(CancellationToken requestAbortedCancellationToken)
        {
            return (_enqueuer != null) && (await _enqueuer.EnqueueAsync(requestAbortedCancellationToken));
        }

        private async Task<bool> ShouldDecrementConcurrentRequestsCountAsync()
        {
            return (_settings.Limit != ConcurrentRequestsLimitSettings.UnlimitedConcurrentRequests)
                && ((_enqueuer == null) || !(await _enqueuer.DequeueAsync()));
        }
    }
}
