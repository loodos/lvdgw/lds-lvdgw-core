﻿using LDS.LVDGW.Core.Services;
using LDS.LVDGW.Core.Settings;
using LDS.LVDGW.Core.Tools;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using LDS.LVDGW.Core.Helpers;

namespace LDS.LVDGW.Core.Middlewares
{
    /// https://github.com/aspnet/ResponseCaching/blob/f8831928de92f7900f26b74f64eb4083227f257a/src/Microsoft.AspNetCore.ResponseCaching/ResponseCachingMiddleware.cs
    /// https://elanderson.net/2017/02/log-requests-and-responses-in-asp-net-core/
    /// https://stackoverflow.com/questions/47181356/c-sharp-dotnet-core-middleware-wrap-response

    public class ResponseCacheMiddleware
    {
        public const string HTTPHEADER_CACHE_CREATED = "X-ResponseCache-Created";
        public const string HTTPHEADER_CACHE_HASHKEY = "X-ResponseCache-Hash-Key";

        private readonly RequestDelegate _next;
        private readonly ResponseCacheSettings _settings;
        private readonly ICacheService _cache;

        private static readonly ILogger Log = Serilog.Log.ForContext<ResponseCacheMiddleware>();

        public ResponseCacheMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, ICacheService cache)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _settings = appSettings.Value?.ResponseCache ?? throw new ArgumentNullException(nameof(ResponseCacheSettings));

            _cache = cache;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var request = httpContext.Request;

            if (IsRequestCacheable(request))
            {
                var hashKey = HttpUtility.UrlEncode(CreateHashKey(request,_settings.AcceptedHeadersForHashKey));
                var cacheKey = CreateCacheKey(request, hashKey);
                var cachedResponse = await _cache.GetAsync<CachedResponse>(cacheKey);

                if (cachedResponse != null)
                {
                    httpContext.Response.StatusCode = cachedResponse.StatusCode;
                    httpContext.Response.ContentLength = cachedResponse.ContentLength;
                    httpContext.Response.ContentType = cachedResponse.ContentType;
                    cachedResponse.Headers.ToList().ForEach(header =>
                    {
                        httpContext.Response.Headers[header.Key] = new StringValues(header.Value);
                    });

                    httpContext.Response.Headers.Add(HTTPHEADER_CACHE_CREATED, cachedResponse.Created.ToString("R"));
                    httpContext.Response.Headers.Add(HTTPHEADER_CACHE_HASHKEY, cachedResponse.HashKey);

                    // write response from cache
                    using (var output = new MemoryStream(cachedResponse.BodyBytes))
                    {
                        await output.CopyToAsync(httpContext.Response.Body);
                    }

                    Log.Debug("Response from cache");
                }
                else
                {

                    // Shim Response Stream and cache response
                    var originalBodyStream = httpContext.Response.Body;

                    try
                    {
                        using (var responseBodyStream = new MemoryStream())
                        {
                            httpContext.Response.Body = responseBodyStream;

                            await _next(httpContext);

                            if (IsResponseCacheable(httpContext.Response))
                            {
                                await CacheResponseAsync(cacheKey, hashKey, httpContext);
                            }
                            httpContext.Response.Body.Seek(0, SeekOrigin.Begin);

                            // Unshim Response Stream
                            await responseBodyStream.CopyToAsync(originalBodyStream);
                        }
                    }
                    finally
                    {
                        //and finally, reset the stream for downstream calls
                        httpContext.Response.Body = originalBodyStream;
                    }
                }
            }
            else
            {
                await _next(httpContext);
            }
        }

        private bool IsRequestCacheable(HttpRequest request)
        {
            if (_settings.ExcludeHeaders != null && _settings.ExcludeHeaders.Any())
            {
                foreach (var excludeHeader in _settings.ExcludeHeaders)
                {
                    if (request.Headers.ContainsKey(excludeHeader.Key))
                    {
                        if (string.IsNullOrWhiteSpace(excludeHeader.Value)
                            || request.Headers[excludeHeader.Key].Contains(excludeHeader.Value))
                        {
                            return false;
                        }
                    }
                }
            }


            if (_settings.AcceptedVerbs == null || !_settings.AcceptedVerbs.Contains(request.Method))
            {
                return false;
            }

            if (_settings.EndpointRules != null && _settings.EndpointRules.Any())
            {
                foreach (var endpointRule in _settings.EndpointRules.Where(x => x.NoCache))
                {
                    var endpoint = Endpoint.Parse(endpointRule.Endpoint);

                    if (endpoint.IsMatch(request.Path.Value, request.Method))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool IsResponseCacheable(HttpResponse response)
        {
            if (response.StatusCode != StatusCodes.Status200OK)
            {
                return false;
            }

            return true;
        }

        private string CreateCacheKey(HttpRequest request, string hashKey)
        {
            return $"{request.Method}:{request.Path.Value}:{request.QueryString.Value}:{hashKey}";
        }

        private static string CreateHashKey(HttpRequest request,string[] acceptedHeadersForHashKey)
        {
            var httpMethod = new HttpMethod(request.Method);
            var formData = new List<KeyValuePair<string, string>>();
            var rawData = "";
            var headerData = request.Headers.Where(x => acceptedHeadersForHashKey.Contains(x.Key.ToLowerInvariant()))
                .Select(item => new KeyValuePair<string, string>(item.Key, item.Value)).ToList();

            if (request.HasFormContentType)
            {
                formData.AddRange(from item in request.Form from value in item.Value select new KeyValuePair<string, string>(item.Key, value));
            }
            else if (httpMethod != HttpMethod.Get && httpMethod != HttpMethod.Head)
            {
                using (var reader = new StreamReader(request.Body, Encoding.UTF8))
                {
                    rawData = reader.ReadToEnd();
                }
            }

            var pairs = formData.Select(x => $"{x.Key}-{x.Value}");
            var headerPairs = headerData.Select(x => $"{x.Key}-{x.Value}");
            var cnt = string.Join("|", pairs) + rawData + string.Join("|", headerPairs);

            using (var md5 = MD5.Create())
            {
                var result = md5.ComputeHash(Encoding.ASCII.GetBytes(cnt));
                return Encoding.ASCII.GetString(result);
            }
        }

        private async Task CacheResponseAsync(string cacheKey, string hashKey, HttpContext httpContext)
        {
            string responseBody = null;
            httpContext.Response.Body.Seek(0, SeekOrigin.Begin);

            // string response
            if (httpContext.Response.ContentType.Contains("text") || httpContext.Response.ContentType.Contains("json"))
            {
                responseBody = await new StreamReader(httpContext.Response.Body).ReadToEndAsync();
            }
            // binary (image, file, ...) response -> store base64
            else
            {
                using (var streamResponseBody = new MemoryStream())
                {
                    await httpContext.Response.Body.CopyToAsync(streamResponseBody);
                    responseBody = string.Concat("base64,", Convert.ToBase64String(streamResponseBody.ToArray()));
                }
            }

            IDictionary<string, string[]> headers = httpContext.Response.Headers.ToDictionary(header => header.Key, header => header.Value.ToArray(), StringComparer.OrdinalIgnoreCase);

            var cachedResponse = new CachedResponse()
            {
                Created = DateTime.UtcNow,
                HashKey = hashKey,
                StatusCode = httpContext.Response.StatusCode,
                ContentLength = httpContext.Response.ContentLength,
                ContentType = httpContext.Response.ContentType,
                Headers = headers,
                Body = responseBody
            };

            // set expiry from rules
            int expiryInSeconds = _settings.DefaultExpiryInSeconds;
            if (_settings.EndpointRules != null && _settings.EndpointRules.Any())
            {
                foreach (var endpointRule in _settings.EndpointRules)
                {
                    var endpoint = Endpoint.Parse(endpointRule.Endpoint);

                    if (endpoint.IsMatch(httpContext.Request.Path.Value, httpContext.Request.Method))
                    {
                        expiryInSeconds = endpointRule.ExpiryInSeconds;
                        break;
                    }
                }
            }

            await _cache.SetAsync(cacheKey, cachedResponse, expiryInSeconds);
        }
    }

    [Serializable]
    public class CachedResponse
    {
        public DateTimeOffset Created { get; set; }
        public string HashKey { get; set; }
        public int StatusCode { get; set; }
        public long? ContentLength { get; set; }
        public string ContentType { get; set; }
        public IDictionary<string, string[]> Headers { get; set; }
        public string Body { get; set; }

        public byte[] BodyBytes
        {
            get
            {
                if (string.IsNullOrEmpty(Body))
                    return new byte[0];
                else if (Body.StartsWith("base64,"))
                    return Convert.FromBase64String(this.Body.Substring(7));
                else
                    return Encoding.UTF8.GetBytes(this.Body);
            }
        }
    }
}
