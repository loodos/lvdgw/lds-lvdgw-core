﻿using LDS.LVDGW.Plugin.ResponseWrapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using LDS.LVDGW.Core.Settings;
using Serilog;

namespace LDS.LVDGW.Core.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ResponseWrapperSettings _responseWrapperSettings;
        private readonly IResponseWrapperPlugin _responseWrapperPlugin;

        private static readonly ILogger Log = Serilog.Log.ForContext<ErrorHandlingMiddleware>();

        public ErrorHandlingMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings, IServiceProvider serviceProvider)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _responseWrapperSettings = appSettings.Value?.ResponseWrapper ?? throw new ArgumentNullException(nameof(ResponseWrapperSettings));

            if (_responseWrapperSettings.Enable)
            {
                _responseWrapperPlugin = serviceProvider.GetService<IResponseWrapperPlugin>();
            }
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                Log.Error(ex, ex.Message);

                var body = ex.Message;
                var statusCode = GetStatusCode(ex);

                httpContext.Response.StatusCode = statusCode;

                var contentType = "text/plain";

                if (_responseWrapperPlugin != null)
                {
                    var originalBody = body;
                    body = await _responseWrapperPlugin.WrapResponseAsync(originalBody, statusCode, contentType);

                    // responseBody was wrapped
                    if (originalBody != body)
                    {
                        contentType = _responseWrapperSettings.ContentType;
                    }
                }

                httpContext.Response.ContentType = contentType;

                await httpContext.Response.WriteAsync(body);
            }
        }

        private int GetStatusCode(Exception ex)
        {
            if (ex is TimeoutException)
            {
                return StatusCodes.Status408RequestTimeout;
            }
            else
            {
                return StatusCodes.Status500InternalServerError;
            }
        }
    }
}
