﻿using App.Metrics;
using App.Metrics.Gauge;
using App.Metrics.Timer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Extensibility
{
    public static class MetricsRegistry
    {
        public static class PluginMetrics
        {
            private static readonly string ContextName = "Plugin";

            public static TimerOptions TimerExecution = new TimerOptions
            {
                Context = ContextName,
                Name = "Execution Time"
            };
        }

        public static class Gauges
        {
            public static GaugeOptions ConcurrentRequests = new GaugeOptions
            {
                Name = "Concurrent Requests",
                MeasurementUnit = Unit.Calls
            };
        }

    }
}
