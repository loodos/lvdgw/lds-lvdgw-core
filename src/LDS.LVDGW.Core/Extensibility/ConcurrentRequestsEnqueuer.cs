﻿using LDS.LVDGW.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Extensibility
{
    public class ConcurrentRequestsEnqueuer
    {
        private static readonly Task<bool> _enqueueFailedTask = Task.FromResult(false);

        private readonly ConcurrentRequestsLimitSettings _settings;
        private readonly SemaphoreSlim _queueSemaphore = new SemaphoreSlim(1, 1);
        private readonly LinkedList<TaskCompletionSource<bool>> _queue = new LinkedList<TaskCompletionSource<bool>>();

        public ConcurrentRequestsEnqueuer(ConcurrentRequestsLimitSettings concurrentRequestsLimitSettings)
        {
            _settings = concurrentRequestsLimitSettings;
        }

        public async Task<bool> EnqueueAsync(CancellationToken requestAbortedCancellationToken)
        {
            Task<bool> enqueueTask = _enqueueFailedTask;

            if (_settings.QueueSize > 0)
            {
                CancellationToken enqueueCancellationToken = GetEnqueueCancellationToken(requestAbortedCancellationToken);

                try
                {
                    await _queueSemaphore.WaitAsync(enqueueCancellationToken);
                    try
                    {
                        if (_queue.Count < _settings.QueueSize)
                        {
                            enqueueTask = InternalEnqueueAsync(enqueueCancellationToken);
                        }
                        else if (_settings.LimitExceededPolicy == ConcurrentRequestsLimitSettings.LimitExceededPolicies.FifoQueueDropHead)
                        {
                            InternalDequeue(false);

                            enqueueTask = InternalEnqueueAsync(enqueueCancellationToken);
                        }
                    }
                    finally
                    {
                        _queueSemaphore.Release();
                    }
                }
                catch (OperationCanceledException)
                { }
            }

            return await enqueueTask;
        }

        public async Task<bool> DequeueAsync()
        {
            bool dequeued = false;

            await _queueSemaphore.WaitAsync();
            try
            {
                if (_queue.Count > 0)
                {
                    InternalDequeue(true);
                    dequeued = true;
                }
            }
            finally
            {
                _queueSemaphore.Release();
            }

            return dequeued;
        }

        private Task<bool> InternalEnqueueAsync(CancellationToken enqueueCancellationToken)
        {
            Task<bool> enqueueTask = _enqueueFailedTask;

            if (!enqueueCancellationToken.IsCancellationRequested)
            {
                TaskCompletionSource<bool> enqueueTaskCompletionSource = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
                enqueueCancellationToken.Register(CancelEnqueue, enqueueTaskCompletionSource);

                _queue.AddLast(enqueueTaskCompletionSource);
                enqueueTask = enqueueTaskCompletionSource.Task;
            }

            return enqueueTask;
        }

        private CancellationToken GetEnqueueCancellationToken(CancellationToken requestAbortedCancellationToken)
        {
            CancellationToken enqueueCancellationToken = CancellationTokenSource.CreateLinkedTokenSource(
                requestAbortedCancellationToken,
                GetTimeoutToken()
            ).Token;

            return enqueueCancellationToken;
        }

        private CancellationToken GetTimeoutToken()
        {
            CancellationToken timeoutToken = CancellationToken.None;

            if (_settings.QueueTimeoutInSeconds != ConcurrentRequestsLimitSettings.UnlimitedQueueTimeout)
            {
                CancellationTokenSource timeoutTokenSource = new CancellationTokenSource();

                timeoutToken = timeoutTokenSource.Token;

                timeoutTokenSource.CancelAfter(_settings.QueueTimeoutInSeconds);
            }

            return timeoutToken;
        }

        private void CancelEnqueue(object state)
        {
            bool removed = false;

            TaskCompletionSource<bool> enqueueTaskCompletionSource = ((TaskCompletionSource<bool>)state);

            // This is blocking, but it looks like this callback can't be asynchronous.
            _queueSemaphore.Wait();
            try
            {
                removed = _queue.Remove(enqueueTaskCompletionSource);
            }
            finally
            {
                _queueSemaphore.Release();
            }

            if (removed)
            {
                enqueueTaskCompletionSource.SetResult(false);
            }
        }

        private void InternalDequeue(bool result)
        {
            TaskCompletionSource<bool> enqueueTaskCompletionSource = _queue.First.Value;

            _queue.RemoveFirst();

            enqueueTaskCompletionSource.SetResult(result);
        }
    }
}
