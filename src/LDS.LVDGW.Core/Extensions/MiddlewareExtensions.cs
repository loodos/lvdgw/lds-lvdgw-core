﻿using LDS.LVDGW.Core.Middlewares;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDS.LVDGW.Core.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseSecurityHeaders(this IApplicationBuilder app)
        {
            return app.UseMiddleware<SecurityHeadersMiddleware>();
        }

        public static IApplicationBuilder UseResponseCache(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ResponseCacheMiddleware>();
        }

        public static IApplicationBuilder UseResponseWrapper(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ResponseWrapperMiddleware>();
        }

        public static IApplicationBuilder UseAppAuthentication(this IApplicationBuilder app)
        {
            return app.UseMiddleware<AppAuthenticationMiddleware>();
        }

        public static IApplicationBuilder UseUserAuthentication(this IApplicationBuilder app)
        {
            return app.UseMiddleware<UserAuthenticationMiddleware>();
        }

        public static IApplicationBuilder UseEnrichment(this IApplicationBuilder app)
        {
            return app.UseMiddleware<EnrichmentMiddleware>();
        }

        public static IApplicationBuilder UseMicroservices(this IApplicationBuilder app)
        {
            return app.UseMiddleware<MicroservicesMiddleware>();
        }

        public static IApplicationBuilder UseConcurrentRequestsLimit(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ConcurrentRequestsLimitMiddleware>();
        }

        public static IApplicationBuilder UseIPRestriciton(this IApplicationBuilder app)
        {
            return app.UseMiddleware<IPRestricitonMiddleware>();
        }

        public static IApplicationBuilder UseErrorHandling(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ErrorHandlingMiddleware>();
        }

        public static IApplicationBuilder UseDiagnosticLog(this IApplicationBuilder app)
        {
            return app.UseMiddleware<DiagnosticLogMiddleware>();
        }

        public static IApplicationBuilder UsePrivateFileServer(this IApplicationBuilder app)
        {
            return app.UseMiddleware<PrivateFileServerMiddleware>();
        }

        
    }
}
