﻿using LDS.LVDGW.Core.Middlewares;
using LDS.LVDGW.Core.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Routing.Template;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace LDS.LVDGW.Core.Extensions
{
    public static class RoutingEngineExtensions
    {
        private static readonly ILogger Log = Serilog.Log.ForContext(typeof(RoutingEngineExtensions));

        public static IApplicationBuilder UseRoutingEngine(this IApplicationBuilder app, RoutingEngineSettings routingEngineSettings)
        {
            /// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-2.1#using-routing-middleware

            var routeBuilder = new RouteBuilder(app, new RouteHandler(RoutingEngineHandler(routingEngineSettings)));

            // Map route from configuration
            foreach (var rule in routingEngineSettings.Routes.Where(x => x.Enable))
            {
                routeBuilder.MapRoute(rule.Name, rule.Template);
            }
            // Map route for catch-all
            routeBuilder.MapRoute("LDS-LVDGW-CATCH-ALL", "{*prm}");

            var routes = routeBuilder.Build();
            app.UseRouter(routes);
            return app;
        }

        private static RequestDelegate RoutingEngineHandler(RoutingEngineSettings routingEngineSettings)
        {
            return async (httpContext) =>
            {
                Uri destinationUri = GetDestinationUri(httpContext.GetRouteData(), httpContext.Request.Query, routingEngineSettings.Routes);

                if (destinationUri != null)
                {
                    // Request destination, write response
                    await RouteRequestAsync(httpContext.Request, httpContext.Response, destinationUri, routingEngineSettings.TimeoutInSeconds);

                    //context.Response.StatusCode = StatusCodes.Status200OK;
                    //await context.Response.WriteAsync(
                    //    $"RoutingEngine[{currentRouteName}] -> Destination: {destination} RouteValues: {string.Join(", ", currentRouteValues)}");
                }
                else
                {
                    // not found response with 404 status code
                    httpContext.Response.StatusCode = StatusCodes.Status404NotFound;
                    httpContext.Response.ContentType = "text/plain";
                    await httpContext.Response.WriteAsync($"RoutingEngine[?] -> NotFound");
                }
            };
        }

        private static Uri GetDestinationUri(RouteData routeData, IQueryCollection requestQueryCollection, List<RoutingEngineSettings.Route> routes)
        {
            Uri destinationUri = null;

            var currentRouter = routeData.Routers.FirstOrDefault(x => x.GetType() == typeof(Microsoft.AspNetCore.Routing.Route));

            if (currentRouter != null)
            {
                var currentRoute = ((Microsoft.AspNetCore.Routing.Route)currentRouter);
                var matchRule = routes.FirstOrDefault(x => x.Name == currentRoute.Name && x.Enable);

                if (matchRule != null)
                {
                    var currentRouteValues = routeData.Values;

                    var destination = matchRule.Destination;

                    // replace route parameters
                    foreach (var routeValue in currentRouteValues)
                    {
                        destination = destination.Replace($"{{{routeValue.Key}}}", routeValue.Value?.ToString());
                    }

                    var uriBuilder = new UriBuilder(destination);

                    // add query parameters
                    if (requestQueryCollection != null && requestQueryCollection.Count > 0)
                    {
                        var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                        foreach (var requestQuery in requestQueryCollection)
                        {
                            query[requestQuery.Key] = requestQuery.Value;
                        }
                        uriBuilder.Query = query.ToString();
                    }

                    destinationUri = uriBuilder.Uri;

                    Log.Debug("RoutingEngine {0} -> {1}", currentRoute.RouteTemplate, destinationUri.ToString());
                }
            }

            return destinationUri;
        }

        private static async Task RouteRequestAsync(HttpRequest request, HttpResponse response, Uri destinationUri, int timeoutInSeconds)
        {
            // TODO: Simulation, slow response
            //System.Threading.Thread.Sleep(500);

            using (var client = new HttpClient())
            {
                var httpMethod = new HttpMethod(request.Method);
                var requestMessage = new HttpRequestMessage(httpMethod, destinationUri);
                var requestContent = new MultipartFormDataContent();

                client.Timeout = TimeSpan.FromSeconds(timeoutInSeconds);
                var isJsonContent = false;

                // HEADERS
                foreach (var header in request.Headers)
                {
                    if (header.Key.Equals("Accept", StringComparison.InvariantCultureIgnoreCase)
                        || header.Key.Equals("Accept-Encoding", StringComparison.InvariantCultureIgnoreCase)
                        || header.Key.Equals("Host", StringComparison.InvariantCultureIgnoreCase)
                        || header.Key.Equals("If-None-Match", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }
                    else if (header.Key.Equals("Content-Type", StringComparison.InvariantCultureIgnoreCase))
                    {
                        foreach (var headerValue in header.Value)
                        {
                            client.DefaultRequestHeaders.Accept.TryParseAdd(headerValue);
                            isJsonContent = headerValue == "application/json";
                        }
                    }
                    else
                    {
                        client.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value.AsEnumerable());
                    }
                }

                var jsonBody = "";

                // FROM CONTENT or BODY
                if (request.HasFormContentType)
                {
                    if (request.Form?.Files?.Count <= 0)
                    {
                        var formData = new List<KeyValuePair<string, string>>();
                        foreach (var item in request.Form)
                        {
                            foreach (var value in item.Value)
                            {
                                formData.Add(new KeyValuePair<string, string>(item.Key, value));
                            }
                        }

                        requestMessage.Content = new FormUrlEncodedContent(formData);
                    }
                    else if (request.Form?.Files?.Count > 0)
                    {
                        foreach (var item in request.Form)
                        {
                            requestContent.Add(new StringContent(item.Value), item.Key);
                        }

                        foreach (var item in request.Form.Files)
                        {
                            var streamContent = new StreamContent(item.OpenReadStream());
                            var fileContent = new ByteArrayContent(streamContent.ReadAsByteArrayAsync().Result);
                            requestContent.Add(fileContent, item.Name, item.FileName);
                        }
                    }
                }
                else
                {
                    // send body if form content not exists
                    if (httpMethod != HttpMethod.Get && httpMethod != HttpMethod.Head)
                    {
                        if (isJsonContent)
                        {
                            using (var reader = new StreamReader(request.Body, Encoding.UTF8))
                            {
                                jsonBody = await reader.ReadToEndAsync();
                            }
                        }
                        else
                        {
                            requestMessage.Content = new StreamContent(request.Body);
                        }
                    }
                }


                // SEND REQUEST
                HttpResponseMessage responseMessage = null;
                try
                {
                    if (request.HasFormContentType && request.Form?.Files?.Count > 0)
                    {
                        responseMessage = await client.PostAsync(destinationUri, requestContent);
                    }
                    else
                    {
                        if (isJsonContent)
                        {
                            responseMessage = await client.PostAsync(destinationUri, new StringContent(jsonBody, Encoding.UTF8, "application/json"));
                        }
                        else
                        {
                            responseMessage = await client.SendAsync(requestMessage);
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    throw new TimeoutException();
                }

                // RESPONSE STATUS
                response.StatusCode = (int)responseMessage.StatusCode;

                // RESPONSE HEADERS
                foreach (var header in responseMessage.Headers)
                {
                    if (header.Key == "Transfer-Encoding")
                        continue;

                    response.Headers[header.Key] = new StringValues(header.Value.ToArray());
                }

                // CONTENT-TYPE
                response.ContentType = responseMessage.Content.Headers.ContentType?.ToString();

                // RESPONSE BODY
                await responseMessage.Content.CopyToAsync(response.Body);

                //string responseContent = await responseMessage.Content.ReadAsStringAsync();
                //await context.Response.WriteAsync(responseContent);
            }
        }


    }
}
