using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using App.Metrics;
using App.Metrics.Formatters;
using App.Metrics.Formatters.Prometheus;
using App.Metrics.AspNetCore;
using App.Metrics.AspNetCore.Endpoints;
using App.Metrics.AspNetCore.Tracking;
using App.Metrics.Filtering;
using App.Metrics.Formatters.Json;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Serilog;
using LDS.LVDGW.Core.Settings;

namespace LDS.LVDGW.Core
{
    /// Serilog
    /// https://github.com/serilog/serilog-settings-configuration/blob/dev/sample/Sample/appsettings.json
    /// 

    public class Program
    {
        // Build configuration from appsettings
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();

        // webhost
        public static IWebHostBuilder WebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                    .UseKestrel()
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseConfiguration(Configuration)
                    .UseSerilog()
                    .UseStartup<Startup>();

        public static int Main(string[] args)
        {
            if (args.Contains("--load-plugins"))
            {
                LoadPlugins();
                return 0;
            }

            ConfigureLogger();

            try
            {
                Log.Information("LDS.LVDGW running...");

                var webHostbuilder = WebHostBuilder(args);
                webHostbuilder = ConfigureAndUseMetrics(webHostbuilder);

                // Build and run webhost
                webHostbuilder.Build().Run();

                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "LDS.LVDGW terminated unexpectedly");

                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        // Generate plugin install command for entrypoint.sh
        public static void LoadPlugins()
        {
            List<PluginSettings> pluginSettings = new List<PluginSettings>();
            Configuration.GetSection("Plugins").Bind(pluginSettings);

            if (pluginSettings != null && pluginSettings.Count > 0)
            {
                List<string> commandList = new List<string>();
                foreach (var settings in pluginSettings)
                {
                    string command = $"dotnet add package {settings.Nuget.Name} --version {settings.Nuget.Version} --source {settings.Nuget.Source}";
                    commandList.Add(command);
                }

                Console.WriteLine(string.Join(" && ", commandList));
            }
        }

        // Configure Logger
        public static void ConfigureLogger()
        {
            // Configure Serilog logger
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.ConfigurationSection(Configuration.GetSection("Logging"))
                .CreateLogger();

            Serilog.Debugging.SelfLog.Enable(Console.Error);
        }

        public static IWebHostBuilder ConfigureAndUseMetrics(IWebHostBuilder hostBuilder)
        {
            MetricsMonitoringSettings settings = new MetricsMonitoringSettings();
            Configuration.GetSection("MetricsMonitoring").Bind(settings);

            var metricsBuilder = AppMetrics.CreateDefaultBuilder()
                .Configuration.Configure(options =>
                {
                    options.Enabled = settings.Enable;
                    options.DefaultContextLabel = "LDS.LVDGW";
                    options.ReportingEnabled = settings.ReportTo.Enable;
                })
                .OutputEnvInfo.AsJson()
                .OutputMetrics.AsPrometheusPlainText()
                .OutputMetrics.AsJson();

            if (settings.ReportTo.Enable)
            {
                if (settings.ReportTo.Console != null)
                {
                    metricsBuilder = metricsBuilder.Report.ToConsole(options => {
                        options.FlushInterval = settings.ReportTo.Console.FlushInterval;
                    });
                }
                if (settings.ReportTo.TextFile != null)
                {
                    metricsBuilder = metricsBuilder.Report.ToTextFile(options => {
                        options.FlushInterval = settings.ReportTo.TextFile.FlushInterval;
                        options.AppendMetricsToTextFile = settings.ReportTo.TextFile.Append;
                        options.OutputPathAndFileName = settings.ReportTo.TextFile.FilePath;
                    });
                }
            }

            var Metrics = metricsBuilder.Build();

            hostBuilder = hostBuilder.ConfigureMetrics(Metrics);

            if (settings.Enable)
            {
                hostBuilder = hostBuilder.ConfigureAppMetricsHostingConfiguration(options =>
                {
                    options.AllEndpointsPort = settings.Port;
                    options.EnvironmentInfoEndpoint = settings.EnvironmentInfoEndpoint;
                    options.MetricsEndpoint = settings.MetricsPrometheusEndpoint;
                    options.MetricsTextEndpoint = settings.MetricsJSONEndpoint;
                })
                .UseMetricsWebTracking(options => {
                    options.ApdexTrackingEnabled = true;
                    options.ApdexTSeconds = settings.ApdexTSeconds;
                    options.IgnoredHttpStatusCodes = settings.IgnoredHttpStatusCodes;
                    options.IgnoredRoutesRegexPatterns = settings.IgnoredRoutesRegexPatterns;
                    options.OAuth2TrackingEnabled = false;
                })
                .UseMetricsEndpoints(options =>
                {
                    options.EnvironmentInfoEndpointEnabled = true;
                    options.MetricsEndpointEnabled = true;
                    options.MetricsTextEndpointEnabled = true;

                    //Prometheus output formatting
                    //options.EnvInfoEndpointOutputFormatter = Metrics.OutputEnvFormatters.GetType<EnvInfoJsonOutputFormatter>();
                    options.MetricsEndpointOutputFormatter = Metrics.OutputMetricsFormatters.GetType<MetricsPrometheusTextOutputFormatter>();
                    options.MetricsTextEndpointOutputFormatter = Metrics.OutputMetricsFormatters.GetType<MetricsJsonOutputFormatter>();
                });
            }
            
            return hostBuilder;
        }

    }
}
