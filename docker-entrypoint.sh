#!/bin/bash

set -ex

cd /app
COMMAND_LOAD_PLUGIN=$( dotnet LDS.LVDGW.Core.dll --load-plugins )

cd /src/

dotnet --info
dotnet clean

eval $COMMAND_LOAD_PLUGIN

dotnet restore -v n
dotnet build --configuration Release
dotnet publish --configuration Release --output /app

cd /app
exec dotnet LDS.LVDGW.Core.dll
