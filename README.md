﻿# Loodos Vanilla Delivery GateWay - LVDGW

//TODO: add badges

## What is it

//TODO: write description

## Middlewares

- Response Cache
- Security Headers
- Routing Engine
- Public/Private File Server
- Diagnostic logging
- Global Error Handling
- Centralized Log Monitoring
- Analytics/Metrics Monitoring
- Response Wrapper
- Concurrent Requests Limitation
- Custom Request Enrichment
- IP Restriciton
- APP Authentication
- User Authentication
- Rate Limiting
- Serving Custom Microservice 

//TODO: write description each middleware

## Plugin based architecture

//TODO: write description

## Configuration

//TODO: write description in wiki

## Build & Run

//TODO: write description in wiki

Create docker image for local development:

`docker build -t lds-lvdgw-core:debug -f .\src\LDS.LVDGW.Core\Dockerfile .`

`docker build --force-rm --no-cache -t lds-lvdgw-core:debug -f .\src\LDS.LVDGW.Core\Dockerfile .`

## Deploy

//TODO: write description in wiki

## Who is using this

//TODO: add projects which is using this

## Commercial Support

//TODO: write description

## Contributing

//TODO: write description

## License

Loodos Vanilla Delivery Gateway (LVDGW) is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://loodos.com)
//TODO: add licence file link

## Acknowledgements

Thanks for providing free open source licensing

- [Serilog](https://github.com/serilog)

//TODO: add dependencies

